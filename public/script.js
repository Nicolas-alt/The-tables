//Toggle class for plus buttom
const button_plus = document.getElementById('button_plus');
const div_buttons = document.getElementById('div--buttons');

button_plus.addEventListener('click', togle_plus);

function togle_plus()
{
    div_buttons.classList.toggle('div--buttons--visible');
    button_plus.classList.toggle('button--plus--open');
}

//Toggle simple form
const formTarget = document.getElementById('formTarget');
const buttonTarget = document.getElementById('buttonTarget');
buttonTarget.addEventListener('click', show_formTarget);

function show_formTarget()
{
    formTarget.classList.toggle('form-target--block');
}

//Toggle double form

const formSearch = document.getElementById('formSearch');
const buttonSlider = document.getElementById('buttonSlider');

buttonSlider.addEventListener('click', show_formDobule);

function show_formDobule()
{
    formSearch.classList.toggle('form-search--block');
}

//Show all tables range 1 - 20
const buttonShowAll = document.getElementById('button--showAll');
buttonShowAll.addEventListener('click', showAll);

const sectionResultsJs = document.getElementById('section--resultsJs');

function showAll()
{
    for(let i = 1; i <= 20; i++){
        let numId = i;
        let numTable = i;
        let table =
        ` 
        <div id="div--tableContainer" class="div--tableContainer">
            <div class="div--tittleTable">
                <h2>Tabla del ${numTable}</h2>
            </div>
            <ul id="ul_table-${numId}" class="ul--list">
            </ul>
        </div>
        `;
        sectionResultsJs.innerHTML += table;

        let ul = document.getElementById(`ul_table-${numId}`);

        let num = i;
        for( let ii = 0; ii <= 10; ii++)
        {
            let aumento = ii;
            let resultado = num * aumento;
            let texto = `${num} x ${aumento} = ${resultado}`;

            let li = document.createElement('li');
            li.className = 'li--item';
            li.textContent = texto;

            ul.appendChild(li);
        }//For create text
    }//For create structure table

    const buttonPannel = document.getElementById('section_buttons');

    const MakebuttonDelete = `<button id="button_delete" class="button--deleteAll"><i class='bx bxs-trash bx-md' style='color:#ff0000'></i></button>`;
    buttonPannel.innerHTML += MakebuttonDelete;

    const  buttonDelete = document.getElementById('button_delete');
    buttonDelete.addEventListener('click', cleanArea);

    button_plus.addEventListener('click', togle_plus);

}//Show all function

function cleanArea()
{
    const buttonPannel = document.getElementById('section_buttons');
    const  buttonDelete = document.getElementById('button_delete');
    sectionResultsJs.remove();
    buttonPannel.removeChild(buttonDelete);
}

//Formulario simple
//14
formTarget.addEventListener('submit', formDataTargetDraw);

function formDataTargetDraw(e)
{
    e.preventDefault();
    let valorInputSearch = document.getElementById('inputNumber').value;
    createTableSimple(valorInputSearch);
}

//Imprimir tabla
function createTableSimple(valorInputSearch)
{
    
    let numTable = valorInputSearch;
    let table =
    ` 
    <div id="div--tableContainer" class="div--tableContainer">
        <div class="div--tittleTable">
            <h2>Tabla del ${numTable}</h2>
            <i id="i-delete" class='i--deleteTable bx bx-plus bx-md' style='color:#ee0000'></i>
        </div>
        <ul id="ul_table" class="ul--list">
        </ul>
    </div>
    `;
    sectionResultsJs.innerHTML += table;

    let ul = document.getElementById('ul_table');

    let num = parseInt(valorInputSearch);
    for( let i = 0; i <= 10; i++)
    {
        let aumento = i;
        let resultado = num * aumento;
        let texto = `${num} x ${aumento} = ${resultado}`;

        let li = document.createElement('li');
        li.className = 'li--item';
        li.textContent = texto;

        ul.appendChild(li);
    }

    const i_delete = document.getElementById('i-delete');

    i_delete.addEventListener('click',delete_item);

    function delete_item()
    {
        const div_tableContainer = document.getElementById('div--tableContainer');
        div_tableContainer.remove();
    }
}

//Formulario doble
//25
formSearch.addEventListener('submit',showTables);

function showTables(e)
{
    e.preventDefault();
    let tableA = document.getElementById('inputRange1').value;
    let tableB = document.getElementById('inputRange2').value;

    convertNumbers(tableA, tableB);
    function  convertNumbers(tableA, tableB)
    {
        let table1 = parseInt(tableA);
        let table2 = parseInt(tableB);
        
        if( table1 > table2)
        {

            let NewNum1 = table2;
            let NewNum2 = table1;
            createTableRange(NewNum1,NewNum2);
        }//IF
        createTableRange(table1, table2);
    }
}


function createTableRange(table1,table2){

    for(let num1 = table1; num1 <= table2; num1++){

        let numId = num1;
        let table =
        ` 
        <div id="div--tableContainer" class="div--tableContainer">
            <div class="div--tittleTable">
                <h2>Tabla del ${numId}</h2>
            </div>
            <ul id="ul_table-${numId}" class="ul--list">
            </ul>
        </div>
        `;
        sectionResultsJs.innerHTML += table;

        let ul = document.getElementById(`ul_table-${numId}`);

        let num = numId;
        for( let ii = 0; ii <= 10; ii++)
        {
            let aumento = ii;
            let resultado = num * aumento;
            let texto = `${num} x ${aumento} = ${resultado}`;

            let li = document.createElement('li');
            li.className = 'li--item';
            li.textContent = texto;

            ul.appendChild(li);
        }//For create text
    }//For create structure table
}//Create table range

//Pendiente a crear
function errorMessage()
{
    
}
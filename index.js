const PORT = process.env || 5000;
const express = require('express');
const app = express();

const http = require('http');

app.use(express.static(__dirname + '/public/index.html'));

app.listen(PORT, function() {
  console.log('Servidor web escuchando en el puerto 3000');
});